﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class admin_panel : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_id"] == null && Session["admin_pass"] == null)
            {
                Response.Redirect("admin_sign_in");
            }
        }

        protected void log_out_btn_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("admin_sign_in");
        }

        protected void add_new_book_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("add_new_book");
        }

        protected void dashboard_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("admin");
        }

        protected void add_new_category_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("add_new_category");
        }

        protected void modify_book_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("modify_book");
        }
    }
}