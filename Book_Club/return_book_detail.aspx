﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin_panel.Master" CodeBehind="return_book_detail.aspx.cs" Inherits="Book_Club.return_book_detail" %>

<asp:Content ID="Content6" ContentPlaceHolderID="return_book_detail_page" Runat="Server" >
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header">
                          <h3>Book Return Details</h3>
                    </div>
                        <h2 class="text-center text-info">
                            <asp:Label ID="message" runat="server"></asp:Label>
                        </h2>

                        <div class="panel panel-info">
                            <div class="panel-body">
                        <div class="form-group">
                            <label for="user_name">User Name: </label>
                            <asp:Label ID="use_name" runat="server"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label for="user_id">User Id: </label>
                            <asp:Label ID="user_id" runat="server"></asp:Label>
                        </div>
                        <asp:HiddenField ID="Hidden_User_Id" runat="server" />
                        <asp:HiddenField ID="Hidden_Book_Id" runat="server" />
                        <asp:HiddenField ID="borrow_Info_Id" runat="server" />            
                            <div class="form-group">
                                <label for="book_name">Book Name: </label>
                                <asp:Label ID="b_name" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label for="issue_date">Issue Date: </label>
                                <asp:Label ID="issue_date" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label for="return_date">Return Date: </label>
                                <asp:TextBox ID="return_date" runat="server" type="date" CssClass="form-control"></asp:TextBox>
                            </div>
                                <div class="form-group">
                                <label for="delay_fee">Delay Fee (In BDT) : </label>
                                    <asp:TextBox ID="delay_fee" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            <div class="form-group">
                                <asp:Button ID="return_book_btn" runat="server" CssClass="btn btn-sm btn-info" Text="Save" OnClick="return_book_btn_Click" />
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
        </asp:Content>