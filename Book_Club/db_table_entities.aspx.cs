﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public class User_Info
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string user_id { get; set; }
        public int dept { get; set; }
        public string password { get; set; }
        public int type { get; set; }
        public int borrowed_book_quantity { get; set; }
    }

    public class Book_Info
    {
        public int category_id { get; set; }
        public string name { get; set; }
        public string author_name { get; set; }
        public int quantity { get; set; }
        public DateTime adding_date { get; set; }
    }

    public class User_Borrow_Info
    {
        public int user_id { get; set; }
        public int book_id { get; set; }
        public DateTime borrow_date { get; set; }
        public DateTime return_date { get; set; }
        public float delay_fee { get; set; }
    }
}