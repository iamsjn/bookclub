﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class back_end : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null && Session["user_pass"] == null)
            {
                Response.Redirect("sign_in.aspx");
            }
        }

        protected void log_out_btn_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("sign_in.aspx");
        }

        protected void dashboard_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("user_panel");
        }
    }
}