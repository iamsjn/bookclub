﻿    using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class return_book_detail : System.Web.UI.Page
    {
        User_Info_Db_Data_Access data = new User_Info_Db_Data_Access();
        User_Borrow_Info_Db_Data_Access data_2 = new User_Borrow_Info_Db_Data_Access();
        Book_Info_Db_Data_Access data_3 = new Book_Info_Db_Data_Access();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["borrow_user_id"] != null && Session["book_id"] != null)
            {
                SqlDataReader user_info = data.Search_user_id(Session["borrow_user_id"].ToString());

                if (user_info.HasRows)
                {
                    while (user_info.Read())
                    {

                        this.use_name.Text = user_info.GetString(1) + " " + user_info.GetString(2);
                        this.user_id.Text = user_info.GetString(4);
                        this.Hidden_User_Id.Value = user_info.GetInt32(0).ToString();
                        this.Hidden_Book_Id.Value = Session["book_id"].ToString();
                        this.b_name.Text = Session["book_name"].ToString();

                        SqlDataReader user_borrow_info = data_2.Search_user_borrow_info(this.Hidden_User_Id.Value, this.Hidden_Book_Id.Value);

                        if(user_borrow_info.HasRows)
                        {
                            while(user_borrow_info.Read())
                            {
                                if (DateTime.Now.Subtract(user_borrow_info.GetDateTime(3)).TotalDays>7 && user_info.GetByte(7)==0)
                                {
                                    this.delay_fee.Text = (DateTime.Now.Subtract(user_borrow_info.GetDateTime(3)).TotalDays * 5).ToString();
                                }

                                else
                                {
                                    this.delay_fee.Text = "0.00";
                                }
                                this.issue_date.Text = user_borrow_info.GetDateTime(3).ToShortDateString();
                            }
                        }

                        else
                        {
                            this.message.Text = "No record found for this user !";
                        }
                    }
                }

            }

            else
            {
                Response.Redirect("admin");
            }
        }

        protected void return_book_btn_Click(object sender, EventArgs e)
        {
                User_Borrow_Info obj = new User_Borrow_Info();

                obj.user_id = Convert.ToInt32(this.Hidden_User_Id.Value);
                obj.book_id = Convert.ToInt32(this.Hidden_Book_Id.Value);

                if (data_2.Update_user_borrow_info(this.Hidden_User_Id.Value, this.Hidden_Book_Id.Value, Convert.ToDouble(Request.Form[delay_fee.UniqueID]), Convert.ToDateTime(Request.Form[return_date.UniqueID])) == true && data.Decrement_user_borrowed_book_quantity(this.Hidden_User_Id.Value) == true && data_3.Increment_book_quantity(this.Hidden_Book_Id.Value) == true)
                {
                    this.message.Text = "Book returned successfully !";
                    Session.Remove("borrow_user_id");
                    Session.Remove("book_id");
                    Session.Remove("book_name");
                }

                else
                {
                    this.message.Text = "Please try again !";
                }
        }
    }
}