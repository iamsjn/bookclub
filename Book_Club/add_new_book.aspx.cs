﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class add_new_book : System.Web.UI.Page
    {
        Book_Info_Db_Data_Access data_2 = new Book_Info_Db_Data_Access();
        Book_Category_Db_Data_Access data = new Book_Category_Db_Data_Access();
        Book_Info obj = new Book_Info();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                SqlDataReader reader = data.Get_category();

                if (reader.HasRows)
                {
                    category_id.DataSource = reader;
                    category_id.DataTextField = "name";
                    category_id.DataValueField = "id";
                    category_id.DataBind();
                }
            }
        }

        protected void add_book_btn_Click(object sender, EventArgs e)
        {
            if (Session["admin_id"] != null && Session["admin_pass"] != null )
            {
                if (string.IsNullOrEmpty(this.book_name.Text))
                {
                    this.book_name_err.Text = "*Book name is required !";
                }

                else if(string.IsNullOrEmpty(this.author_name.Text))
                {
                    this.author_name_err.Text = "*Author name is required !";
                }

                else if(string.IsNullOrEmpty(this.quantity.Text))
                {
                    this.qty_err.Text = "*Quantity is required !";
                }

                else
                {
                    obj.name = this.book_name.Text;
                    this.category_id.ClearSelection();
                    obj.category_id = Convert.ToInt32(Request.Form[category_id.UniqueID]);
                    obj.author_name = this.author_name.Text;
                    obj.quantity = Convert.ToInt32(this.quantity.Text);
                    obj.adding_date = DateTime.Now;
                    if (data_2.Save_book_info(obj) == true)
                    {
                        this.message.Text = "Book successfully saved!";
                    }

                    else
                    {
                        this.message.Text = "Please try again!";
                    }
                }
            }

            else
            {
                Response.Redirect("admin");
            }
        }
    }
}