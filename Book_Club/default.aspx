﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/front_end.Master" CodeBehind="default.aspx.cs" Inherits="Book_Club._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="default_page" Runat="Server" >
    <div class="container" style="padding-top:10%;">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                    <asp:TextBox ID="search_box" runat="server" CssClass="form-control"  Placeholder="Search Book by Name, Category Name, Author Name"></asp:TextBox>
                    </div>
                        <div class="form-group">
                        <asp:Button CssClass="btn btn-sm btn-info pull-right" ID="search_btn" runat="server" Text="Search" />
                        </div>
                        <br /><br /><br /><br />
                    </div>
                        <asp:GridView ID="search_result_grid" runat="server" CssClass="table table-hover table-bordered form-group col-md-12 col-sm-12 col-xs-12" AutoGenerateColumns="False" CellSpacing="25" ToolTip="Book List" OnSelectedIndexChanged="search_result_grid_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField DataField="book_name" HeaderText="Book Name" HeaderStyle-CssClass="text-center" >
<HeaderStyle CssClass="text-center"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="book_author_name" HeaderText="Author Name" HeaderStyle-CssClass="text-center" >
<HeaderStyle CssClass="text-center"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="book_status" HeaderText="Available Status" HeaderStyle-CssClass="text-center" >
<HeaderStyle CssClass="text-center"></HeaderStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
              </div>
        </div>
    </div>
</div>
</asp:Content>  
