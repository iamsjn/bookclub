﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class edit_book : System.Web.UI.Page
    {
        Book_Info_Db_Data_Access data_2 = new Book_Info_Db_Data_Access();
        Book_Category_Db_Data_Access data = new Book_Category_Db_Data_Access();
        Book_Info obj = new Book_Info();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Session["book_id"] != null)
            {
                SqlDataReader reader = data.Get_category();

                if (reader.HasRows)
                {
                    category_id.DataSource = reader;
                    category_id.DataTextField = "name";
                    category_id.DataValueField = "id";
                    category_id.DataBind();

                    category_id.SelectedValue = Session["book_id"].ToString();
                }

                SqlDataReader reader_2 = data_2.Get_book_info(Session["book_id"].ToString());

                if(reader_2.HasRows)
                {
                    while(reader_2.Read())
                    {
                        this.book_name.Text = reader_2.GetString(2);
                        this.author_name.Text = reader_2.GetString(3);
                        this.quantity.Text = reader_2.GetInt32(4).ToString();
                        category_id.SelectedValue = reader_2.GetInt32(1).ToString();
                    }
                }

            }
        }

        protected void edit_book_btn_Click(object sender, EventArgs e)
        {
            if (Session["admin_id"] != null && Session["admin_pass"] != null)
            {
                if (string.IsNullOrEmpty(this.book_name.Text))
                {
                    this.book_name_err.Text = "*Book name is required !";
                }

                else if (string.IsNullOrEmpty(this.author_name.Text))
                {
                    this.author_name_err.Text = "*Author name is required !";
                }

                else if (string.IsNullOrEmpty(this.quantity.Text))
                {
                    this.qty_err.Text = "*Quantity is required !";
                }

                else
                {
                    obj.name = this.book_name.Text;
                    obj.category_id = Convert.ToInt32(Request.Form[category_id.UniqueID]);
                    obj.author_name = this.author_name.Text;
                    obj.quantity = Convert.ToInt32(this.quantity.Text);
                    if (data_2.Update_book_info(obj, Session["book_id"].ToString()) == true)
                    {
                        this.message.Text = "Book successfully updated!";
                        Session.Remove("book_id");
                    }

                    else
                    {
                        this.message.Text = "Please try again!";
                    }
                }
            }

            else
            {
                Response.Redirect("admin");
                Session.Remove("book_id");
            }
        }
    }
}