﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public class Book_Category_Db_Data_Access
    {
        public bool Save_category(string category_name)
        {
            string sql = "INSERT INTO book_category_information VALUES ('" + category_name + "')";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public SqlDataReader Get_category()
        {
            string sql = "SELECT * FROM book_category_information";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }
    }
}