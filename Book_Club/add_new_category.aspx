﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin_panel.Master" CodeBehind="add_new_category.aspx.cs" Inherits="Book_Club.add_new_category" %>

<asp:Content ID="Content8" ContentPlaceHolderID="add_new_category_page" Runat="Server" >
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header">
                          <h3>Add New Book Category</h3>
                    </div>
                        <h2 class="text-center text-info">
                            <asp:Label ID="message" runat="server"></asp:Label>
                        </h2>

                        <div class="panel panel-info">
                            <div class="panel-body">
                        <div class="form-group">
                            <label for="category_name">Category Name: </label>
                            <asp:TextBox ID="category_name" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:Label ID="name_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>    
                            <div class="form-group">
                                <asp:Button ID="return_book_btn" runat="server" CssClass="btn btn-sm btn-info" Text="Save" OnClick="return_book_btn_Click"/>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
        </asp:Content>
