﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin_panel.Master" CodeBehind="modify_book.aspx.cs" Inherits="Book_Club.modify_book" %>
<asp:Content ID="Content7" ContentPlaceHolderID="modify_book_page" Runat="Server">
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header">
                          <h3>Search A Book To Modify</h3>
                    </div>
                        <h2 class="text-center text-info">
                            <asp:Label ID="message" runat="server"></asp:Label>
                        </h2>
                            <div class="panel panel-info">
                                <div class="panel-body">
                                        <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group">
                                        <asp:TextBox ID="search_box" runat="server" CssClass="form-control"  Placeholder="Search Book by Name, Category Name, Author Name"></asp:TextBox>
                                        </div>
                                            <div class="form-group">
                                            <asp:Button CssClass="btn btn-sm btn-info pull-right" ID="search_btn" runat="server" Text="Search" />
                                            </div>
                                            <br /><br /><br />
                                        </div>
                                            <asp:GridView ID="search_result_grid" runat="server" CssClass="form-group col-md-12 col-sm-12 col-xs-12 table table-hover table-bordered" AutoGenerateColumns="False" CellSpacing="25" ToolTip="Book List" OnRowCommand="search_result_grid_RowCommand">
                                                <Columns>
                                                    <asp:BoundField DataField="book_id" HeaderText="#" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden"/>

                                                    <asp:BoundField DataField="book_name" HeaderText="Book Name" HeaderStyle-CssClass="text-center" >
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="book_author_name" HeaderText="Author Name" HeaderStyle-CssClass="text-center" >
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="book_status" HeaderText="Available Status" HeaderStyle-CssClass="text-center" >
                                                    </asp:BoundField>

                                                    <asp:ButtonField Text="Edit" ControlStyle-CssClass="btn btn-xs btn-warning" ItemStyle-CssClass="text-center" CommandName="edit_btn">
                                                    </asp:ButtonField>

                                                    <asp:ButtonField Text="Delete" ControlStyle-CssClass="btn btn-xs btn-danger" ItemStyle-CssClass="text-center" CommandName="delete_btn">
                                                    </asp:ButtonField>
                                                </Columns>
                                            </asp:GridView>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</asp:Content>
