﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class user_panel : System.Web.UI.Page
    {
        User_Borrow_Info_Db_Data_Access data = new User_Borrow_Info_Db_Data_Access();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["id"] != null && Session["user_name"] != null)
            {
               SqlDataReader reader = data.Show_user_borrow_info(Convert.ToInt32(Session["id"]));

               borrow_history_grid.DataSource = reader;
               borrow_history_grid.DataBind();
            }
        }
    }
}