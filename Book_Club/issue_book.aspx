﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin_panel.Master" CodeBehind="issue_book.aspx.cs" Inherits="Book_Club.issue_book" %>

<asp:Content ID="Content3" ContentPlaceHolderID="issue_book_page" Runat="Server" >
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header">
                          <h3>Issue A New Book</h3>
                    </div>
                        <div class="form-group">
                            <asp:Label ID="user_id_err" runat="server" ForeColor="Red"></asp:Label>
    <asp:TextBox ID="user_id" CssClass="form-control" runat="server" placeholder="Enter User Id"></asp:TextBox>
                            </div>
                        <div class="form-group">
    <asp:Button ID="user_id_btn" runat="server" Text=" Go " CssClass="btn btn-sm btn-info pull-right" OnClick="user_id_btn_Click"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </asp:Content>
