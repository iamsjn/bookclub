﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class add_new_category : System.Web.UI.Page
    {
        Book_Category_Db_Data_Access data = new Book_Category_Db_Data_Access();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void return_book_btn_Click(object sender, EventArgs e)
        {
            if (Session["admin_id"] != null && Session["admin_pass"] != null)
            {
                if(string.IsNullOrEmpty(this.category_name.Text))
                {
                    this.name_err.Text = "*Category name is required !";
                }

                else
                {
                    if (data.Save_category(this.category_name.Text) == true)
                    {
                        this.message.Text = "Category successfully saved!";
                    }

                    else
                    {
                        this.message.Text = "Please try again!";
                    }
                }
            }

            else
            {
                Response.Redirect("admin");
            }
        }
    }
}