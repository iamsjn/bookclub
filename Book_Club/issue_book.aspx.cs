﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class issue_book : System.Web.UI.Page
    {
        User_Info_Db_Data_Access data = new User_Info_Db_Data_Access();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void user_id_btn_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(this.user_id.Text))
            {
                this.user_id_err.Text = "Please Enter a Id.";
            }

            else
            {
                string user_id = this.user_id.Text;
                SqlDataReader info = data.Search_user_id(user_id);

                if (info.HasRows)
                {
                    while(info.Read())
                    {
                        Session["borrow_user_id"] = info.GetString(4);
                        Response.Redirect("issue_book_detail");
                    }
                }

                else
                {
                    this.user_id_err.Text = "Id Not Found!.";
                }

            }
        }
    }
}