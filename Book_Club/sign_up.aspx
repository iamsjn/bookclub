﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/front_end.Master" CodeBehind="sign_up.aspx.cs" Inherits="Book_Club.sign_up" %>

<asp:Content ID="Content2" ContentPlaceHolderID="sign_up_page" Runat="Server" >
    <div class="container">
        <div class="col-md-12">
<div class="page-header">
  <h1>Sign Up<br /><small> and get a free membership</small></h1>
</div>
            <div class="panel panel-info">
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <asp:TextBox ID="first_name" runat="server" CssClass="form-control" placeholder="Enter Your First Name"></asp:TextBox>
                            <asp:Label ID="first_name_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <asp:TextBox ID="last_name" runat="server" CssClass="form-control" placeholder="Enter Your Last Name"></asp:TextBox>
                            <asp:Label ID="last_name_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <asp:TextBox ID="email" runat="server" CssClass="form-control" placeholder="Enter Your Email"></asp:TextBox>
                            <asp:Label ID="email_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <asp:RadioButtonList ID="type" runat="server">
                                <asp:ListItem Value="1">
                                    &nbsp; Faculty
                                </asp:ListItem>
                                <asp:ListItem Value="0">
                                    &nbsp; Student
                                </asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Label ID="type_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label for="id">Id</label>
                            <asp:TextBox ID="id" runat="server" CssClass="form-control" placeholder="Enter Your Id"></asp:TextBox>
                            <asp:Label ID="id_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label for="dept">Department</label>
                            <asp:DropDownList ID="dept" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0">
                                    Select Your Dept.
                                </asp:ListItem>
                                <asp:ListItem Value="1">
                                    ENG
                                </asp:ListItem>
                                <asp:ListItem Value="2">
                                    CS
                                </asp:ListItem>
                                <asp:ListItem Value="3">
                                    BBA
                                </asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="dept_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <asp:TextBox ID="password" runat="server" CssClass="form-control" placeholder="Enter Your Password" TextMode="Password"></asp:TextBox>
                            <asp:Label ID="password_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label for="re_password">Re-Password</label>
                            <asp:TextBox ID="re_password" runat="server" CssClass="form-control" placeholder="Re-Password" TextMode="Password"></asp:TextBox>
                            <asp:Label ID="re_password_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="submit" runat="server" Text="Sign Up !" class="btn btn-sm btn-info" OnClick="submit_Click"/>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>