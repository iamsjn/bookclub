﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;

namespace Book_Club
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("AdminRoute", "admin","~/admin_panel.aspx");
            routes.MapPageRoute("AdminSignInRoute", "admin_sign_in", "~/admin_sign_in.aspx");
            routes.MapPageRoute("BookIssueRoute", "issue_book", "~/issue_book.aspx");
            routes.MapPageRoute("BookIssueDetailRoute", "issue_book_detail", "~/issue_book_detail.aspx");
            routes.MapPageRoute("BookReturnRoute", "return_book", "~/return_book.aspx");
            routes.MapPageRoute("BookReturnDetailRoute", "return_book_detail", "~/return_book_detail.aspx");
            routes.MapPageRoute("AddNewBookRoute", "add_new_book", "~/add_new_book.aspx");
            routes.MapPageRoute("AddNewCategoryRoute", "add_new_category", "~/add_new_category.aspx");
            routes.MapPageRoute("ModifyBookRoute", "modify_book", "~/modify_book.aspx");
            routes.MapPageRoute("DeleteBookRoute", "delete_book", "~/delete_book.aspx");
            routes.MapPageRoute("EditBookRoute", "edit_book", "~/edit_book.aspx");
            routes.MapPageRoute("SignInRoute", "sign_in", "~/sign_in.aspx");
            routes.MapPageRoute("SignUpRoute", "sign_up", "~/sign_up.aspx");
            routes.MapPageRoute("UserPanelRoute", "user_panel", "~/user_panel.aspx");
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}