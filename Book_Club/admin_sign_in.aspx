﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/front_end.Master" CodeBehind="admin_sign_in.aspx.cs" Inherits="Book_Club.admin_sign_in" %>

<asp:Content ID="Content4" ContentPlaceHolderID="admin_sign_in_page" Runat="Server" >
    <div class="container" style="padding-top:2%;">
        <div class="col-md-6 col-md-offset-3">
<div class="page-header">
  <h1>Sign In</h1>
</div>
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="form-group">
                <label for="id">
                <asp:Label ID="id_err_lbl" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <asp:Label ID="pass_err_lbl" runat="server" ForeColor="Red"></asp:Label>
                <br />
                Id<br />
                </label>
                &nbsp;<asp:TextBox ID="id" runat="server" CssClass="form-control" placeholder="Your Id"></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <asp:TextBox ID="password" runat="server" CssClass="form-control" placeholder="Your Password" TextMode="Password"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Button ID="sign_in_btn" CssClass="btn btn-sm btn-info" runat="server" Text="Sign In" OnClick="sign_in_btn_Click"/>
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
        </div>
    </div>
    </asp:Content>
