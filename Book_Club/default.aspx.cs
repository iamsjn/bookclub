﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Book_Club
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                string search_key = this.search_box.Text.ToString();
                Book_Info_Db_Data_Access obj = new Book_Info_Db_Data_Access();
                SqlDataReader reader = obj.Search_book(search_key);

                search_result_grid.DataSource = reader;
                search_result_grid.DataBind();
            }
        }

        protected void search_result_grid_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}