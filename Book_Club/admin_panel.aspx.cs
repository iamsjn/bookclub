﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class admin_panel1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string search_key = this.search_box.Text.ToString();
                Book_Info_Db_Data_Access obj = new Book_Info_Db_Data_Access();
                SqlDataReader reader = obj.Search_book(search_key);

                search_result_grid.DataSource = reader;
                search_result_grid.DataBind();
            }

        }

        protected void search_result_grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "issue_btn")
            {
                int index = Convert.ToInt32(e.CommandArgument);

                GridViewRow selectedRow = search_result_grid.Rows[index];
                TableCell book_id = selectedRow.Cells[0];
                TableCell book_name = selectedRow.Cells[1];
                Session["book_id"] = book_id.Text;
                Session["book_name"] = book_name.Text;

                Response.Redirect("issue_book");
            }

            if (e.CommandName == "return_btn")
            {
                int index = Convert.ToInt32(e.CommandArgument);

                GridViewRow selectedRow = search_result_grid.Rows[index];
                TableCell book_id = selectedRow.Cells[0];
                TableCell book_name = selectedRow.Cells[1];
                Session["book_id"] = book_id.Text;
                Session["book_name"] = book_name.Text;

                Response.Redirect("return_book");
            }
        }
    }
}