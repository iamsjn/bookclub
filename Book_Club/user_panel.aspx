﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/back_end.Master" CodeBehind="user_panel.aspx.cs" Inherits="Book_Club.user_panel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="user_panel" Runat="Server" >
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>
                            <asp:Label ID="user_name" runat="server"></asp:Label>
                        </h1>
                        <div class="page-header">
                          <h3>Borrow History</h3>
                    </div>
                    <asp:GridView ID="borrow_history_grid" runat="server" CssClass="table table-bordered form-group col-md-12 col-sm-12 col-xs-12" AutoGenerateColumns="False" CellSpacing="25" ToolTip="Borrow History">
                                                <Columns>
                                                    <asp:BoundField DataField="name" HeaderText="Book Name" HeaderStyle-CssClass="text-center" >

                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="author_name" HeaderText="Author Name" HeaderStyle-CssClass="text-center" >

                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="user_borrow_date" Dataformatstring="{0:MM/dd/yyyy}" HeaderText="Borrowed Date" HeaderStyle-CssClass="text-center" >

                                                    </asp:BoundField>

                                                    <asp:BoundField HeaderText="Returned Date" DataField="user_return_date" Dataformatstring="{0:MM/dd/yyyy}" HeaderStyle-CssClass="text-center">

                                                    </asp:BoundField>

                                                    <asp:BoundField HeaderText="Delay Fee (In BDT)" HeaderStyle-CssClass="text-center" DataField="delay_fee">

                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    </asp:Content>
