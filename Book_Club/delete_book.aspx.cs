﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class delete_book : System.Web.UI.Page
    {
        User_Borrow_Info_Db_Data_Access data = new User_Borrow_Info_Db_Data_Access();
        Book_Info_Db_Data_Access data_2 = new Book_Info_Db_Data_Access();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void no_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("modify_book");
        }

        protected void yes_btn_Click(object sender, EventArgs e)
        {
            if(Session["book_id"] != null)
            {
                SqlDataReader reader = data.Search_book_borrow_info(Session["book_id"].ToString());

                if(reader.HasRows)
                {
                    this.message.Visible = false;
                    this.no_btn.Visible = false;
                    this.yes_btn.Visible = false;
                    this.delete_warn.Text = "Sorry this book has borrow history, first take return this book from user then delete this book.";
                    Session.Remove("book_id");
                }

                else
                {
                    if(data_2.Delete_book_info(Session["book_id"].ToString()))
                    {
                        this.message.Visible = false;
                        this.no_btn.Visible = false;
                        this.yes_btn.Visible = false;
                        this.delete_warn.Text = "Book has successfully deleted !";
                        Session.Remove("book_id");
                    }

                    else
                    {
                        this.delete_warn.Text = "Please try again !";
                    }
                }
            }

            else
            {
                Response.Redirect("admin");
            }
        }
    }
}