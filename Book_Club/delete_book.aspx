﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin_panel.Master" CodeBehind="delete_book.aspx.cs" Inherits="Book_Club.delete_book" %>

<asp:Content ID="Content8" ContentPlaceHolderID="delete_book_page" Runat="Server" >
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <h3 class="text-danger text-center">
                                    <asp:Label ID="delete_warn" runat="server"></asp:Label>
                                </h3>
                                <h3 class="text-danger text-center">
                                    <asp:Label ID="message" runat="server" Text="Are you sure want to delete this book permanently ?"></asp:Label>
                                </h3>
                                <h2 class="text-center">
                                <asp:Button ID="no_btn" runat="server" CssClass="btn btn-sm btn-success glyphicon-align-center" Text="No" OnClick="no_btn_Click" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="yes_btn" runat="server" CssClass="btn btn-sm btn-danger" Text="Yes" OnClick="yes_btn_Click" />
                                </h2>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </asp:Content>
