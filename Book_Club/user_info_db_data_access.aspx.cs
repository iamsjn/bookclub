﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public class User_Info_Db_Data_Access
    {
        public bool Save_user_info(User_Info obj)
        {
            string sql = "INSERT INTO user_information VALUES ('" + obj.first_name + "', '" + obj.last_name + "', '" + obj.email + "', '" + obj.user_id + "', '" + obj.dept + "', '" + obj.password + "', '" + obj.type + "', '" + obj.borrowed_book_quantity + "')";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public SqlDataReader Check_user(string user_id, string user_pass)
        {
            string sql = "SELECT * FROM user_information WHERE user_id='"+ user_id +"' AND password='"+ user_pass +"'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public SqlDataReader Search_user_id(string user_id)
        {
            string sql = "SELECT * FROM user_information WHERE user_id='" + user_id + "'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public bool Increment_user_borrowed_book_quantity(string user_id)
        {
            string sql = "UPDATE user_information SET borrowed_book_quantity += '" + 1 + "' WHERE id ='" + user_id + "'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public bool Decrement_user_borrowed_book_quantity(string user_id)
        {
            string sql = "UPDATE user_information SET borrowed_book_quantity -= '" + 1 + "' WHERE id ='" + user_id + "'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

    }
}