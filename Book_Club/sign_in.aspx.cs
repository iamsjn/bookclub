﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class sign_in : System.Web.UI.Page
    {
        User_Info_Db_Data_Access data = new User_Info_Db_Data_Access();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] != null && Session["user_pass"] != null)
            {
                Response.Redirect("user_panel");
            }
            else
            {
                return;
            }
        }

        protected void sign_in_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.id.Text) && string.IsNullOrEmpty(this.password.Text))
            {
                this.id_err_lbl.Text = "Please Enter Your Id.";
                this.pass_err_lbl.Text = "Please Enter Your Paasword.";
            }

            else if (string.IsNullOrEmpty(this.id.Text))
            {
                this.id_err_lbl.Text = "Please Enter Your Id.";
            }

            else if (string.IsNullOrEmpty(this.password.Text))
            {
                this.pass_err_lbl.Text = "Please Enter Your Paasword.";
            }

            else
            {
                string user_id = this.id.Text;
                string user_pass = this.password.Text;
                SqlDataReader info = data.Check_user(user_id, user_pass);
                if (info.HasRows)
                {
                    while(info.Read())
                    {
                        Session["id"] = info.GetInt32(0);
                        Session["user_name"] = info.GetString(1);
                        Session["user_email"] = info.GetString(3);
                        Session["user_id"] = info.GetString(4);
                        Session["user_pass"] = info.GetString(6);
                        Session["user_type"] = info.GetByte(7);
                    }
                    Response.Redirect("user_panel");
                }
                else
                {
                    this.id_err_lbl.Text = "Id and Password Combination doesn't match!";
                }
            }
        }


    }
}