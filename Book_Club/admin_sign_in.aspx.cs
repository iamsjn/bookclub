﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class admin_sign_in : System.Web.UI.Page
    {
        Admin_Info_Db_Data_Access data = new Admin_Info_Db_Data_Access();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_id"] != null && Session["admin_pass"] != null)
            {
                Response.Redirect("admin");
            }

            else
            {
                return;
            }
        }

        protected void sign_in_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.id.Text) && string.IsNullOrEmpty(this.password.Text))
            {
                this.id_err_lbl.Text = "Please Enter Your Id.";
                this.pass_err_lbl.Text = "Please Enter Your Paasword.";
            }

            else if (string.IsNullOrEmpty(this.id.Text))
            {
                this.id_err_lbl.Text = "Please Enter Your Id.";
            }

            else if (string.IsNullOrEmpty(this.password.Text))
            {
                this.pass_err_lbl.Text = "Please Enter Your Paasword.";
            }

            else
            {
                string admin_id = this.id.Text;
                string admin_pass = this.password.Text;
                SqlDataReader info = data.Check_admin(admin_id, admin_pass);
                if (info.HasRows)
                {
                    while (info.Read())
                    {
                        Session["admin_id"] = info.GetString(1);
                        Session["admin_pass"] = info.GetString(2);
                    }
                    Response.Redirect("admin");
                }
                else
                {
                    this.id_err_lbl.Text = "Id and Password Combination doesn't match!";
                }
            }
        }
    }
}