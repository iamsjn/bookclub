﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public partial class sign_up : System.Web.UI.Page
    {
        User_Info_Db_Data_Access data = new User_Info_Db_Data_Access();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submit_Click(object sender, EventArgs e)
        {
            string f_name = this.first_name.Text;
            string l_name = this.last_name.Text;
            string email = this.email.Text;
            string id = this.id.Text;
            string pass = this.password.Text;
            string re_pass = this.re_password.Text;

            if(this.First_name_validator(f_name) && this.Last_name_validator(l_name) && this.Email_validator(email) && this.Id_validator(id) && this.Password_validator(pass, re_pass) && this.User_type_validator() && this.User_department_validator())
            {
                    User_Info obj = new User_Info();

                    obj.first_name = this.first_name.Text.ToString();
                    obj.last_name = this.last_name.Text.ToString();
                    obj.email = this.email.Text.ToString();
                    obj.user_id = this.id.Text.ToString();
                    obj.dept = Convert.ToByte(this.dept.SelectedValue);
                    obj.password = this.password.Text;
                    obj.type = Convert.ToByte(this.type.SelectedValue);
                    obj.borrowed_book_quantity = 0;

                    if (data.Save_user_info(obj) == true)
                    {
                        Session["user_name"] = this.first_name.Text;
                        Session["user_email"] = this.email.Text;
                        Session["user_id"] = this.id.Text;
                        Session["user_pass"] = this.password.Text;
                        Session["user_type"] = Convert.ToByte(this.type.SelectedValue);
                        Response.Redirect("user_panel");
                    }
            }
        }

        protected bool First_name_validator(string f_name)
        {
            if (string.IsNullOrEmpty(f_name))
            {
                this.first_name_err.Text = "*Please enter your first name.";
                return false;
            }

            else
            {
                this.first_name_err.Text = "";
                return true;
            }
        }

        protected bool Last_name_validator(string l_name)
        {
            if (string.IsNullOrEmpty(l_name))
            {
                this.last_name_err.Text = "*Please enter your last name.";
                return false;
            }

            else
            {
                this.last_name_err.Text = "";
                return true;
            }
        }

        protected bool Email_validator(string email)
        {
            int i;
            int at_pos = -1;
            int dot_pos = -1;
            int count_at = 0;

            for(i=0; i<email.Length; i++)
            {
                if (email[i] == '.')
                    dot_pos = i;

                if(email[i] == '@')
                {
                    at_pos = i;
                    count_at++;
                }

            }

            if(string.IsNullOrEmpty(email))
            {
                this.email_err.Text = "*Please enter your email.";
                return false;
            }

            else if(at_pos>1 && count_at==1 && (at_pos<dot_pos-1))
            {
                this.email_err.Text = "";
                return true;
            }

            else
            {
                this.email_err.Text = "*Invalid email address.";
                return false;
            }
        }

        protected bool Id_validator(string id)
        {
            SqlDataReader check_id = data.Search_user_id(this.id.Text);

            if(string.IsNullOrEmpty(id))
            {
                this.id_err.Text = "*Please enter your id.";
                return false;
            }

            else if(id.Length<6)
            {
                this.id_err.Text = "*Id must be 6 chars in length.";
                return false;
            }

            else if (check_id.HasRows)
            {
                this.id_err.Text = "*Id must be unique.";
                return false;
            }

            else
            {
                this.id_err.Text = "";
                return true;
            }
        }

        protected bool Password_validator(string pass, string re_pass)
        {
            if(string.IsNullOrEmpty(pass))
            {
                this.password_err.Text = "*Please enter your password.";
                return false;
            }

            else if(pass.Length<4)
            {
                this.password_err.Text = "*Password must be 4 chars in length.";
                return false;
            }

            else if(pass != re_pass)
            {
                this.re_password_err.Text = "*Please Enter the same password again.";
                return false;
            }

            else
            {
                this.re_password_err.Text = "";
                this.password_err.Text = "";
                return true;
            }
        }

        protected bool User_type_validator()
        {
            if(this.type.SelectedIndex == -1)
            {
                this.type_err.Text = "*Please select your designation.";
                return false;
            }

            else
            {
                this.type_err.Text = "";
                return true;
            }
        }

        protected bool User_department_validator()
        {
            if(Convert.ToInt32(this.dept.SelectedValue) <= 0)
            {
                this.dept_err.Text = "*Please select your department.";
                return false;
            }

            else
            {
                this.dept_err.Text = "";
                return true;
            }
        }
    }
}