﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public class Book_Info_Db_Data_Access
    {
        public SqlDataReader Search_book(string search_key)
        {
            string sql = "SELECT book_information.id as book_id, book_information.name as book_name, book_information.author_name as book_author_name,  CASE WHEN book_information.quantity = 0 THEN 'No' ELSE 'Yes' END as book_status FROM book_information JOIN book_category_information ON book_information.category_id = book_category_information.id WHERE book_information.name like '%" + search_key + "%' OR book_information.author_name like '%" + search_key + "%' OR book_category_information.name like '%" + search_key + "%'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public bool Decrement_book_quantity(string book_id)
        {
            string sql = "UPDATE book_information SET quantity -= '" + 1 + "' WHERE id='" + book_id + "'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public bool Increment_book_quantity(string book_id)
        {
            string sql = "UPDATE book_information SET quantity += '" + 1 + "' WHERE id='" + book_id + "'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public bool Save_book_info(Book_Info obj)
        {
            string sql = "INSERT INTO book_information VALUES ('" + obj.category_id + "', '" + obj.name + "', '" + obj.author_name + "', '" + obj.quantity + "', '" + obj.adding_date + "')";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public bool Delete_book_info(string id)
        {
            string sql = "DELETE FROM book_information WHERE ID= '"+ id +"'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public SqlDataReader Get_book_info(string book_id)
        {
            string sql = "SELECT * FROM book_information WHERE id = '"+ book_id +"'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public bool Update_book_info(Book_Info obj, string book_id)
        {
            string sql = "UPDATE book_information SET category_id = '" + obj.category_id + "', name = '" + obj.name + "', author_name = '" + obj.author_name + "', quantity = '" + obj.quantity + "' WHERE id = '"+ book_id +"'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }
    }
}