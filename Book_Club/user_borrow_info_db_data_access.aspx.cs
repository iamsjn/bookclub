﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Book_Club
{
    public class User_Borrow_Info_Db_Data_Access
    {
        public bool Save_borrow_info(User_Borrow_Info obj)
        {
            string sql = "INSERT INTO user_borrow_information (user_id, book_id, borrow_date, delay_fee) VALUES ('" + obj.user_id + "', '" + obj.book_id + "', '" + obj.borrow_date + "', '" + obj.delay_fee + "')";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public SqlDataReader Search_user_borrow_info(string user_id, string book_id)
        {
            string sql = "SELECT * FROM user_borrow_information WHERE user_id='" + user_id + "' AND book_id='" + book_id + "' AND return_date IS NULL";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public SqlDataReader Show_user_borrow_info(int user_id)
        {
            string sql = "SELECT user_borrow_information.id as user_borrow_id, user_borrow_information.borrow_date as user_borrow_date, user_borrow_information.return_date as user_return_date, book_information.name as name, book_information.author_name as author_name, user_borrow_information.delay_fee as delay_fee FROM user_borrow_information JOIN book_information ON user_borrow_information.book_id = book_information.id WHERE user_borrow_information.user_id = '" + user_id + "'";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public bool Update_user_borrow_info(string user_id, string book_id, double delay_fee, DateTime return_date)
        {
            string sql = "UPDATE user_borrow_information SET delay_fee = '" + delay_fee + "', return_date = '" + return_date + "' WHERE user_id ='" + user_id + "' AND book_id ='" + book_id + "' AND return_date IS NULL";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();

            return true;
        }

        public SqlDataReader Search_book_borrow_info(string book_id)
        {
            string sql = "SELECT * FROM user_borrow_information WHERE book_id='" + book_id + "' AND return_date IS NULL";
            string connectionString = ConfigurationManager.ConnectionStrings["book_club"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }
    }
}