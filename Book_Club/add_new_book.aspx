﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin_panel.Master" CodeBehind="add_new_book.aspx.cs" Inherits="Book_Club.add_new_book" %>
<asp:Content ID="Content7" ContentPlaceHolderID="add_new_book_page" Runat="Server" >
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header">
                          <h3>Add New Book</h3>
                    </div>
                        <h2 class="text-center text-info">
                            <asp:Label ID="message" runat="server"></asp:Label>
                        </h2>

                        <div class="panel panel-info">
                            <div class="panel-body">

                        <div class="form-group">
                            <label for="book_name">Book Name: </label>
                            <asp:TextBox ID="book_name" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:Label ID="book_name_err" runat="server" ForeColor="Red"></asp:Label>
                        </div>    
                            <div class="form-group">
                                <label for="author_name">Author Name (Multiple Author Seperate By Comma (,)): </label>
                                <asp:TextBox ID="author_name" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:Label ID="author_name_err" runat="server" ForeColor="Red"></asp:Label>
                            </div>
                        <div class="form-group">
                            <label for="category_id">Category Name: </label>
                            <asp:DropDownList ID="category_id" runat="server" CssClass="form-control" ViewStateMode="Enabled"></asp:DropDownList>
                        </div>
                            <div class="form-group">
                                <label for="quantity">Quantity: </label>
                                <asp:TextBox ID="quantity" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:Label ID="qty_err" runat="server" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="add_book_btn" runat="server" CssClass="btn btn-sm btn-info" Text="Save" OnClick="add_book_btn_Click"/>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
        </asp:Content>
